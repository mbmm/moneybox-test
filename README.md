# Moneybox iOS Technical Challenge

## The Brief:

To create a 'light' version of the Moneybox app that will allow existing users to login and check their account balance as well as viewing their moneybox savings.

## Implementation

### Key points:

- VIPER architecture.
- Networking layer
- CocoaPods 3rd party dependencies management.
- Animations using Lottie
- Unit and UI Tests

#### Architecture:

The App architecture is based on VIPER and the 'Modules' are generated using [Generamba](https://github.com/rambler-digital-solutions/Generamba).
The generator will create all the VIPER component, in some cases not all of them are necessary and will remain empty, however I kept them in the project since will describe the VIPER module in its completeness and it will be ready for future changes whitout adding anny other source file. 


#### Networking layer

The networking layer represent the API client of the given service. The API client responses objects are designed to be high level and adjustable in the future, incapsulating the parsed entity in Result objects so the API client interface will never change if the response of specific endpoint change, reducing the code that need to be changed in the rest of the app, only the entities incapsulated will change (in number and structure).

#### Dependencies management.

In such a simple project there is not much need of 3rd party libraries, however since it is a requirement I decided to used CocoaPods as dependencies manager for 3rd party libraries.

#### Animations

I'm sure you are familiar with the animation I have included in the project :)
Lottie is a very powerfull animation library , it makes animations very easy and fill a big gap on the vector support in hte Apple UI frameworks.

#### Unit and UI Tests

Few unit and UI tests have been written for this project, they are there not to cover the whole project but just to give an indication of tests structure

### What I would add to this or done better ?

With a litle bit of more time I would have written more comments and documentation, there are cases when a little bit more logic is required, like checking if the maximum have been reached before making the payment etc... that I didn't implement due to the limited time.

## Thanks









