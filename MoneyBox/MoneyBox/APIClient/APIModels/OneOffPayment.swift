//
//  OneOffPayment.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 25/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

struct OneOffPayment :JSONModelProtocol {
    
    private static let JSONMoneybox = "Moneybox"
    
    let moneybox :Double // How much the user has saved this week so far and is the users 'Moneybox'

    init(with jsonDictionary: [String : Any]) throws {
     
        guard let jsonMoneybox = jsonDictionary[OneOffPayment.JSONMoneybox] as? Double else {
            throw JSONModelError.missingResponseParameter(parameter: OneOffPayment.JSONMoneybox)
        }
        self.moneybox = jsonMoneybox
    }
}
