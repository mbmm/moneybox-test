//
//  Product.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 25/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

struct Product :JSONModelProtocol {
    
    private static let JSONInvestorProductId        = "InvestorProductId"
    private static let JSONInvestorProductType      = "InvestorProductType"
    private static let JSONProductId                = "ProductId"
    private static let JSONMoneybox                 = "Moneybox"
    private static let JSONSubscriptionAmount       = "SubscriptionAmount"
    private static let JSONPlanValue                = "PlanValue"
    private static let JSONMaximumDeposit           = "MaximumDeposit"
    private static let JSONProduct                  = "Product"
    private static let JSONFriendlyName             = "FriendlyName"
    
    let investorProductId   :Int
    let investorProductType :String
    let productId           :Int
    let moneybox            :Double // How much the user has saved this week so far and is the users 'Moneybox'
    let subscriptionAmount  :Double // What the current weekly subscription is set to
    let planValue           :Double // The current account balance
    let maximumDeposit      :Double // The remaining amount that can be contributed to the current tax year
    let friendlyName        :String
    
    init(with jsonDictionary: [String : Any]) throws {
        
        guard let jsonInvestorProductId = jsonDictionary[Product.JSONInvestorProductId] as? Int else {
            throw JSONModelError.missingResponseParameter(parameter: Product.JSONInvestorProductId)
        }
        self.investorProductId = jsonInvestorProductId
        
        guard let jsonInvestorProductType = jsonDictionary[Product.JSONInvestorProductType] as? String else {
            throw JSONModelError.missingResponseParameter(parameter: Product.JSONInvestorProductType)
        }
        self.investorProductType = jsonInvestorProductType
        
        guard let jsonProductId = jsonDictionary[Product.JSONProductId] as? Int else {
            throw JSONModelError.missingResponseParameter(parameter: Product.JSONProductId)
        }
        self.productId = jsonProductId

        guard let jsonMoneybox = jsonDictionary[Product.JSONMoneybox] as? Double else {
            throw JSONModelError.missingResponseParameter(parameter: Product.JSONMoneybox)
        }
        self.moneybox = jsonMoneybox

        guard let jsonSubscriptionAmount = jsonDictionary[Product.JSONSubscriptionAmount] as? Double else {
            throw JSONModelError.missingResponseParameter(parameter: Product.JSONSubscriptionAmount)
        }
        self.subscriptionAmount = jsonSubscriptionAmount
        
        guard let jsonPlanValue = jsonDictionary[Product.JSONPlanValue] as? Double else {
            throw JSONModelError.missingResponseParameter(parameter: Product.JSONPlanValue)
        }
        self.planValue = jsonPlanValue
        
        guard let jsonMaximumDeposit = jsonDictionary[Product.JSONMaximumDeposit] as? Double else {
            throw JSONModelError.missingResponseParameter(parameter: Product.JSONMaximumDeposit)
        }
        self.maximumDeposit = jsonMaximumDeposit
        
        guard let jsonProductDictionary = jsonDictionary[Product.JSONProduct] as? [String : Any] else {
            throw JSONModelError.missingResponseParameter(parameter: Product.JSONProduct)
        }
        
        guard let jsonFriendlyName = jsonProductDictionary[Product.JSONFriendlyName] as? String else {
            throw JSONModelError.missingResponseParameter(parameter: Product.JSONFriendlyName)
        }
        self.friendlyName = jsonFriendlyName
    }
}
