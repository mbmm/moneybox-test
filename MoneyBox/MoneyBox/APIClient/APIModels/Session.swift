//
//  Session.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 25/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

struct Session : JSONModelProtocol {
    
    private static let JSONBearerToken = "BearerToken"
    
    let bearerToken :String
    
    init(with jsonDictionary: [String : Any]) throws {
        
        guard let jsonBearerToken = jsonDictionary[Session.JSONBearerToken] as? String else {
            throw JSONModelError.missingResponseParameter(parameter: Session.JSONBearerToken)
        }
        self.bearerToken = jsonBearerToken
    }
}
