//
//  User.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 25/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

struct User :JSONModelProtocol {
    
    private static let JSONUserId       = "UserId"
    private static let JSONFirstName    = "FirstName"
    private static let JSONLastName     = "LastName"
    private static let JSONEmail        = "Email"

    let userId      :String
    let firstName   :String
    let lastName    :String
    let email       :String
    
    init(with jsonDictionary: [String : Any]) throws {
        
        guard let jsonUserId = jsonDictionary[User.JSONUserId] as? String else {
            throw JSONModelError.missingResponseParameter(parameter: User.JSONUserId)
        }
        self.userId = jsonUserId
        
        guard let jsonFirstName = jsonDictionary[User.JSONFirstName] as? String else {
            throw JSONModelError.missingResponseParameter(parameter: User.JSONFirstName)
        }
        self.firstName = jsonFirstName
        
        guard let jsonLastName = jsonDictionary[User.JSONLastName] as? String else {
            throw JSONModelError.missingResponseParameter(parameter: User.JSONLastName)
        }
        self.lastName = jsonLastName
        
        guard let jsonEmail = jsonDictionary[User.JSONEmail] as? String else {
            throw JSONModelError.missingResponseParameter(parameter: User.JSONEmail)
        }
        self.email = jsonEmail
    }
}
