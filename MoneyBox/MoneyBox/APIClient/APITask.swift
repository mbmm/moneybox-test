//
//  APITask.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

enum APITaskError :Error {
    case invalidResponse
    case statusCode(code :Int)
}

protocol APITask {    
    func cancel()
}

class  APIBaseTask<Result> :APITask where Result:JSONModelProtocol {
    
    lazy var session: APIURLSession = URLSession.shared
    
    internal var sessionTask :URLSessionTask?
    internal var request :URLRequest
    internal var completionHandler :((Bool, Any?, Error?) -> Void)?
    
    init(request :URLRequest) {        
        self.request = request
    }
    
    func perform() {
        
        sessionTask = session.dataTask(with: request) { [weak self] (data :Data?, response :URLResponse?, requestError :Error?) in
            
            if let result = self?.handle(data: data, response: response, error: requestError) {
                self?.completionHandler?(result.success, result.object, result.error)
            } else {
                self?.completionHandler?(false, nil, nil)
            }
        }
        
        sessionTask?.resume()
    }
    
    func cancel() {
        sessionTask?.cancel()
    }
    
    func handle(data :Data?, response :URLResponse?, error :Error?) -> (success :Bool, object :Any?, error :Error?) {
        
        var result :(success :Bool, object :Any?, error :Error?) = (false, nil, nil)
        
        if let httpResponse = response as? HTTPURLResponse {
            
            if isSuccessCode(httpResponse.statusCode) == false {
                result.error = APITaskError.statusCode(code: httpResponse.statusCode)
            } else {
                result = parse(data: data)
            }
        }
        return result
    }
    
    func parse(data :Data?) -> (success :Bool, object :Any?, error :Error?) {
        
        var success = false
        var model :Any?
        var taskError :Error?
        
        if let resultData = data  {
            
            let jsonObject = try? JSONSerialization.jsonObject(with: resultData, options: [])
            
            if let dictionary = jsonObject as? [String: Any] {
                
                do {
                    model = try Result(with: dictionary)
                    success = true
                } catch {
                    taskError = error
                }
            } else {
                taskError = APITaskError.invalidResponse
            }
        }
        return (success, model, taskError)
    }

    func isSuccessCode(_ code :Int) -> Bool {
        return code == 200 // default success code
    }
}
