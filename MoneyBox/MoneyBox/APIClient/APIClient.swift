//
//  APIClient.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 25/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

enum APIClientError :Error {
    
    case invalidConfiguration
    case unauthorisedCall
}

class APIClient {
    
    var appId       :String?
    var appVersion  :String?
    var apiVersion  :String?
    var host        :String?
    
    private var session :Session?
    private let urlSession: URLSession = URLSession.shared
    private lazy var requestBuilder :APIRequestBuilder?  = {
        
        guard let requestHost = host, let requestBaseUrl = URL(string: requestHost),
                let requestAppVersion = appVersion,
                let requestApiVersion = apiVersion,
                let requestAppId = appId else {
            return nil
        }
        
        return  APIRequestBuilder(requestBaseUrl, requestAppId, requestAppVersion, requestApiVersion)
    }()
    
    static let shared = APIClient()
    
    private init() {}
    
    func login(with email: String, password :String, and idfa :String, completionHandler: @escaping (_ success :Bool, _ result :LoginResult?, _ error :Error?) -> Void) throws -> APITask {
        
        guard let builder = requestBuilder else {
            throw APIClientError.invalidConfiguration
        }
        
        let loginTask = APITaskLogin(with: email, password, idfa, builder: builder)
        loginTask.completionHandler = { [weak self] (_ success :Bool, _ result :Any?, _ error :Error?) in
            
            self?.session = (result as? LoginResult)?.session
            completionHandler(success, result as? LoginResult, error)
        }
        loginTask.perform()
        
        return loginTask
    }
    
    func logout(_ completionHandler: @escaping (_ success :Bool, _ error :Error?) -> Void) throws -> APITask {
        
        guard let builder = requestBuilder else {
            throw APIClientError.invalidConfiguration
        }
        
        guard let userSession = self.session else {
            throw APIClientError.unauthorisedCall
        }
        
        let logoutTask = APITaskLogout(with: userSession, builder: builder)
        logoutTask.completionHandler = { (_ success :Bool, _ :Any?, _ error :Error?) in
        
            completionHandler(success, error)
        }
        logoutTask.perform()
        
        return logoutTask
    }
    
    func thisWeekInvestorProduct(_ completionHandler: @escaping (Bool, ThisWeekInvestorProductResult?, Error?) -> Void) throws -> APITask {
        
        guard let builder = requestBuilder else {
            throw APIClientError.invalidConfiguration
        }
        
        guard let userSession = self.session else {
            throw APIClientError.unauthorisedCall
        }
        
        let thisWeekInvestorProductTask = APITaskThisWeekInvestorProduct(with: userSession, builder: builder)
        thisWeekInvestorProductTask.completionHandler = { (_ success :Bool, _ result :Any?, _ error :Error?) in
            completionHandler(success, result as? ThisWeekInvestorProductResult, error)
        }
        thisWeekInvestorProductTask.perform()
        
        return thisWeekInvestorProductTask
    }
    
    func oneOffPayments(of amount :Double, to product :Product, completionHandler: @escaping (Bool, OneOffPaymentResult?, Error?) -> Void) throws -> APITask {
        
        guard let builder = requestBuilder else {
                throw APIClientError.invalidConfiguration
        }
        
        guard let userSession = self.session else {
            throw APIClientError.unauthorisedCall
        }
        
        let oneOffPaymentTask = APITaskOneOffPayment(with: amount, product: product, session: userSession, builder: builder)
        oneOffPaymentTask.completionHandler = { (_ success :Bool, _ result :Any?, _ error :Error?) in
            completionHandler(success, result as? OneOffPaymentResult, error)
        }
        oneOffPaymentTask.perform()
        
        return oneOffPaymentTask
    }
}
