//
//  APIRequestBuilder.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 25/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

class APIRequestBuilder {
    
    fileprivate static let Header_ContentType           = "Content-Type"
    fileprivate static let Header_ContentTypeJSON       = "application/json"
    fileprivate static let Header_AppId                 = "AppId"
    fileprivate static let Header_AppVersion            = "AppVersion"
    fileprivate static let Header_ApiVersion            = "ApiVersion"
    fileprivate static let Header_Authorization         = "Authorization"
    fileprivate static let Header_AuthorizationBearer   = "Bearer"

    
    static private let timeout = 30.0
    
    private let appId       :String
    private let baseURL     :URL
    private let appVersion  :String
    private let apiVersion  :String
    
    /**
     URLRequest builder
     create and configure request for the API
     */
    init(_ baseURL :URL, _ appId :String, _ appVersion :String, _ apiVersion :String) {
        
        self.baseURL = baseURL
        self.appVersion = appVersion
        self.apiVersion = apiVersion
        self.appId = appId
    }
    
    /**
     build and return a URLRequest for the given endpoint, http method, body and headers
     */
    func build(_ endpoint :String?, _ method :String, _ body :Data?, _ session :Session?) -> URLRequest {
        
        var url = self.baseURL
        
        if let requestEndpoint = endpoint {
            url = url.appendingPathComponent(requestEndpoint)
        }
        
        var request = URLRequest(url: url,
                                 cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData,
                                 timeoutInterval: APIRequestBuilder.timeout)
        
        request.httpMethod = method
        request.httpBody = body
        
        // set base IG API headers
        let baseHeaders = [APIRequestBuilder.Header_ContentType : APIRequestBuilder.Header_ContentTypeJSON,
                           APIRequestBuilder.Header_AppId : self.appId,
                           APIRequestBuilder.Header_ApiVersion : self.apiVersion,
                           APIRequestBuilder.Header_AppVersion : self.appVersion]
        
        request.addHTTPHeaders(headers: baseHeaders)
        
        if let userSession = session {
            request.addHTTPHeaders(headers: [APIRequestBuilder.Header_Authorization : userSession.bearerHeader()])
        }
        
        return request
    }
}

// add all headrers in one go
private extension URLRequest {
    
    mutating func addHTTPHeaders(headers :Dictionary<String, String>) {
        
        for (header, value) in headers {
            
            self.addValue(value, forHTTPHeaderField: header)
        }
    }
}

private extension Session {
    
    func bearerHeader() -> String {
        return APIRequestBuilder.Header_AuthorizationBearer + " " + self.bearerToken
    }
}
