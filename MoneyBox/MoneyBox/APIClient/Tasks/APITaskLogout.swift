//
//  APITaskLogout.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

class APITaskLogout :APIBaseTask<LogoutResult> {
    
    init(with session :Session , builder :APIRequestBuilder) {
        
        let request = builder.build("/users/logout", "POST", nil, session)

        super.init(request: request)
    }
    
    override func parse(data: Data?) -> (success: Bool, object: Any?, error: Error?) {
        
        // ignore the data since is expected an empty response
        return (true, nil, nil)
    }
}
