//
//  APITaskLogin.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

class APITaskLogin :APIBaseTask<LoginResult> {
    
    init(with email :String, _ password :String, _ idfa :String, builder :APIRequestBuilder) {
    
        let parameters :[String : Any] = ["Email"    : email,
                                          "Password" : password,
                                          "Idfa" : idfa]
        
        let body = try? JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions(rawValue: 0))
        
        let request = builder.build("/users/login", "POST", body, nil)
        
        super.init(request: request)
    }
}
