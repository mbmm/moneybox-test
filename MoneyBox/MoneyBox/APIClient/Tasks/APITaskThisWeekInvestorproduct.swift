//
//  APITaskThisWeekInvestorProduct.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

class APITaskThisWeekInvestorProduct :APIBaseTask<ThisWeekInvestorProductResult> {
    
    init(with session :Session , builder :APIRequestBuilder) {
        
        let request = builder.build("/investorproduct/thisweek", "GET", nil, session)
        
        super.init(request: request)
    }
}
