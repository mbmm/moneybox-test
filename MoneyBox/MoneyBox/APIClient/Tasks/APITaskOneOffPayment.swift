//
//  APITaskOneOffPayment.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

class APITaskOneOffPayment :APIBaseTask<OneOffPaymentResult> {
    
    init(with amount :Double, product :Product, session :Session, builder :APIRequestBuilder) {
        
        let parameters :[String : Any] = ["Amount"    : amount,
                                          "InvestorProductId" : product.investorProductId]
        
        let body = try? JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions(rawValue: 0))
        
        let request = builder.build("/oneoffpayments", "POST", body, session)
        
        super.init(request: request)
    }
}
