//
//  OneOffPaymentResult.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

struct OneOffPaymentResult :JSONModelProtocol {
    
    private static let JSONOneOffPayment = "Products"
    
    let oneOffPayment :OneOffPayment
    
    init(with jsonDictionary: [String : Any]) throws {
        
        oneOffPayment = try OneOffPayment(with: jsonDictionary)
    }
}
