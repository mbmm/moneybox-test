//
//  ThisWeekInvestorProductResult.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

struct ThisWeekInvestorProductResult :JSONModelProtocol {
    
    private static let JSONProducts = "Products"
    
    let products :[Product]
    
    init(with jsonDictionary: [String : Any]) throws {
        
        guard let jsonProducts = jsonDictionary[ThisWeekInvestorProductResult.JSONProducts] as? [[String : Any]] else {
            throw JSONModelError.missingResponseParameter(parameter: ThisWeekInvestorProductResult.JSONProducts)
        }
        
        var tmpProducts = [Product]()
        for jsonProduct in jsonProducts {
            tmpProducts.append(try Product(with: jsonProduct))
        }
        
        products = tmpProducts
    }
}
