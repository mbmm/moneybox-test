//
//  LogoutResult.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

struct LogoutResult :JSONModelProtocol {
    init(with jsonDictionary: [String : Any]) throws {}
}
