//
//  LoginResult.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

struct LoginResult :JSONModelProtocol {
 
    private static let JSONUser       = "User"
    private static let JSONSession    = "Session"

    let user        :User
    let session     :Session
    
    init(with jsonDictionary: [String : Any]) throws {
    
        guard let jsonUser = jsonDictionary[LoginResult.JSONUser] as? [String : Any] else {
            throw JSONModelError.missingResponseParameter(parameter: LoginResult.JSONUser)
        }
        user = try User(with: jsonUser)
        
        guard let jsonSession = jsonDictionary[LoginResult.JSONSession] as? [String : Any] else {
            throw JSONModelError.missingResponseParameter(parameter: LoginResult.JSONSession)
        }
        session = try Session(with: jsonSession)
    }
}
