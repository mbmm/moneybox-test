//
//  JSONModelProtocol.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 25/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

enum JSONModelError : Error {
    case missingResponseParameter(parameter :String)
}

/**
 Result model protocol
 */
protocol JSONModelProtocol {
    
    init(with jsonDictionary :[String :Any]) throws
}
