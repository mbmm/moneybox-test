//
//  UIViewController+UIFeedback.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 27/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showError(message :String) {
        
        let alertController = UIAlertController(title: NSLocalizedString("Error", comment: ""),
                                                message: message,
                                                preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""),
                                         style: .cancel,
                                         handler: nil)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showActivityIndicator() {
        
        guard self.presentedViewController == nil else {
            return
        }
        
        let spinnerViewController = SpinnerViewController()
        spinnerViewController.providesPresentationContextTransitionStyle = true
        spinnerViewController.definesPresentationContext = true
        spinnerViewController.modalPresentationStyle = .overCurrentContext
        self.present(spinnerViewController, animated: false, completion: nil)
    }
    
    func hideActivityIndicator() {
        
        if self.presentedViewController?.isKind(of: SpinnerViewController.self) == true {
            self.presentedViewController?.dismiss(animated: false, completion: nil)
        }
    }
}
