//
//  ProductDetailProductDetailConfigurator.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 27/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import UIKit

class ProductDetailModuleConfigurator {

    var product :Product?
    
    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? ProductDetailViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: ProductDetailViewController) {

        let router = ProductDetailRouter()

        let presenter = ProductDetailPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = ProductDetailInteractor()
        interactor.product = product
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
