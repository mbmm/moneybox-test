//
//  ProductDetailProductDetailInteractorOutput.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 27/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

protocol ProductDetailInteractorOutput: class {

    func paymentSuccess()
    func paymentFailure(with error:Error?)
}
