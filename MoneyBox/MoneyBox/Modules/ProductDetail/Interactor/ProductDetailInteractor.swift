//
//  ProductDetailProductDetailInteractor.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 27/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

class ProductDetailInteractor: ProductDetailInteractorInput {

    var product :Product?
    var moneyBox :Double?
    
    var oneOffPaymentTask :APITask?
    
    weak var output: ProductDetailInteractorOutput!

    deinit {
        oneOffPaymentTask?.cancel()
    }
    
    func setProduct(product :Product) {
        self.product = product
    }
    
    //MARK: ProductDetailInteractorInput
    
    func makePayment(of amount: Double) {
        
        guard let paymentProduct = product else {
            return
        }
        
        oneOffPaymentTask?.cancel()
        
        do {
            
            oneOffPaymentTask = try APIClient.shared.oneOffPayments(of: amount, to: paymentProduct, completionHandler: { [weak self] (success :Bool, result :OneOffPaymentResult?, error :Error?) in
                
                if success == true {
                    self?.moneyBox = result?.oneOffPayment.moneybox
                    self?.output.paymentSuccess()
                } else {
                    self?.output.paymentFailure(with: error)
                }
            })
        }
        catch {
            self.output.paymentFailure(with: error)
        }
    }
}
