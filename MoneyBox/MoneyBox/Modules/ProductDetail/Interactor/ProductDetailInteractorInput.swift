//
//  ProductDetailProductDetailInteractorInput.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 27/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

protocol ProductDetailInteractorInput {

    var product :Product? { get set }
    var moneyBox :Double? { get }

    func makePayment(of amount:Double)
}
