//
//  ProductDetailProductDetailPresenter.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 27/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

class ProductDetailPresenter: ProductDetailModuleInput, ProductDetailViewOutput, ProductDetailInteractorOutput {

    weak var view: ProductDetailViewInput!
    var interactor: ProductDetailInteractorInput!
    var router: ProductDetailRouterInput!

    // MARK: ProductDetailViewOutput
    
    func viewIsReady() {
        view.setupInitialState()
    }
    
    func productDisplayName() -> String? {
        return interactor.product?.friendlyName
    }
    
    func productDisplayValue() -> String? {
        
        guard let product = interactor.product else {
            return nil
        }
    
        return String(format: "Plan value: £%0.2f", product.planValue)
    }
    
    func productDisplayMoneyBoxValue() -> String? {
        
        let moneyBox = interactor.moneyBox != nil ? interactor.moneyBox : interactor.product?.moneybox
        
        guard let evaluatedMoneybox = moneyBox else {
            return nil
        }
    
        return String(format: "Your moneybox: £%0.2f", evaluatedMoneybox)
    }
    
    func paymentButtonTapped() {
        
        self.view.showSpinner()
        interactor.makePayment(of: 10.0)
    }
    
    // MARK: ProductDetailInteractorOutput
    
    func paymentSuccess() {
        DispatchQueue.main.async { [weak self] in
            self?.view.reloadMoneyBox()
            self?.view.hideSpinner()
        }
    }
    
    func paymentFailure(with error: Error?) {
        DispatchQueue.main.async { [weak self] in
            self?.view.hideSpinner()
            self?.view.showError(error: error)
        }
    }
}
