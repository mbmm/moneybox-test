//
//  ProductDetailProductDetailViewInput.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 27/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

protocol ProductDetailViewInput: class {

    /**
        @author Manuele Maggi
        Setup initial state of the view
    */

    func setupInitialState()
    
    func reloadMoneyBox()
    
    func showError(error :Error?)
    
    func showSpinner()
    
    func hideSpinner()
}
