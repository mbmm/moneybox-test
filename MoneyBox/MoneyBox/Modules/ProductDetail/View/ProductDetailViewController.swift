//
//  ProductDetailProductDetailViewController.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 27/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import UIKit

class ProductDetailViewController: UIViewController, ProductDetailViewInput {

    @IBOutlet weak var planValueLabel: UILabel!
    @IBOutlet weak var moneyBoxLabel: UILabel!
    @IBOutlet weak var addMoneyButton: UIButton!
    
    var output: ProductDetailViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }

    // MARK: ProductDetailViewInput
    func setupInitialState() {

        self.title = output.productDisplayName()
        planValueLabel.text = output.productDisplayValue()
        moneyBoxLabel.text = output.productDisplayMoneyBoxValue()
    }
    
    func reloadMoneyBox() {
        moneyBoxLabel.text = output.productDisplayMoneyBoxValue()
    }
    
    func showError(error: Error?) {
        self.showError(message: "Payment failed!!!")
    }
    
    func showSpinner() {
        self.showActivityIndicator()
    }
    
    func hideSpinner() {
        self.hideActivityIndicator()
    }
    
    // MARK: Actions
    @IBAction func addMoneyButtonTapped(_ sender: UIButton) {
        output.paymentButtonTapped()
    }
}
