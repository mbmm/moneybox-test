//
//  ProductDetailProductDetailViewOutput.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 27/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

protocol ProductDetailViewOutput {

    /**
        @author Manuele Maggi
        Notify presenter that view is ready
    */

    func viewIsReady()
    
    func paymentButtonTapped()

    func productDisplayName() -> String?
    
    func productDisplayValue() -> String?
    
    func productDisplayMoneyBoxValue() -> String?    
}
