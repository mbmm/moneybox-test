//
//  LoginLoginInteractor.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

class LoginInteractor: LoginInteractorInput {

    weak var output: LoginInteractorOutput!
    
    var loginTask: APITask?
    
    deinit {
        loginTask?.cancel()
    }
    
    // MARK: LoginInteractorInput
    
    func login(email: String, password: String, idfa: String) {
        
        loginTask?.cancel()
        
        do {
            loginTask = try APIClient.shared.login(with: email, password: password, and: idfa) { [weak self] (success :Bool, result :LoginResult?, error :Error?) in
                
                if success {
                    self?.output.loginSuccess()
                } else {
                    self?.output.loginFailure(with: error)
                }
            }
        }
        catch {
            self.output.loginFailure(with: error)
        }
    }
}
