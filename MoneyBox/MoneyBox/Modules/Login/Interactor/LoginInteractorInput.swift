//
//  LoginLoginInteractorInput.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

protocol LoginInteractorInput {

    func login(email: String, password :String, idfa :String)
}
