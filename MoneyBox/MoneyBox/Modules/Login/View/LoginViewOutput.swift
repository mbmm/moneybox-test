//
//  LoginLoginViewOutput.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

protocol LoginViewOutput {

    /**
        @author Manuele Maggi
        Notify presenter that view is ready
    */

    func viewIsReady()
    
    func login()
}
