//
//  LoginLoginViewController.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import UIKit
import AdSupport

class LoginViewController: UIViewController, LoginViewInput {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwrdTextField: UITextField!
    
    var output: LoginViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }

    // MARK: LoginViewInput
    func setupInitialState() {
    }
    
    func showSpinner() {
        self.showActivityIndicator()
    }
    
    func hideSpinner() {
        self.hideActivityIndicator()
    }
    
    func showError(error: Error?) {
        self.showError(message: "Login failed!!!")
    }
    
    var email: String? {
        get { return emailTextField.text }
        set { emailTextField.text = newValue }
    }
    
    var password: String? {
        
        get { return passwrdTextField.text }
        set { passwrdTextField.text = newValue }
    }
    
    var idfa: String? {
        
        get {
            guard ASIdentifierManager.shared().isAdvertisingTrackingEnabled else {
                return nil
            }
            return ASIdentifierManager.shared().advertisingIdentifier.uuidString
        }
    }
    
    // MARK: IBActions
    
    @IBAction func loginButtonAction(_ sender: UIButton) {
        output.login()
    }
}
