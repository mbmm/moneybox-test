//
//  LoginLoginViewInput.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

protocol LoginViewInput: class {

    /**
        @author Manuele Maggi
        Setup initial state of the view
    */

    var email       :String? { get set }
    var password    :String? { get set }
    var idfa        :String? { get }
    
    func setupInitialState()
    
    func showError(error :Error?)
    
    func showSpinner()
    
    func hideSpinner()
}
