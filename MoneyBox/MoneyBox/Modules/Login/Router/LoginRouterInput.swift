//
//  LoginLoginRouterInput.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import UIKit

protocol LoginRouterInput {

    weak var viewController :UIViewController? { get set }
    
    func presentProductList()
}
