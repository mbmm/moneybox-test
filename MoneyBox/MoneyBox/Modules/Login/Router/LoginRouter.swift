//
//  LoginLoginRouter.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//
import UIKit

class LoginRouter: LoginRouterInput {

    // MARK: LoginRouterInput
    
    weak var viewController: UIViewController?
    
    func presentProductList() {
        
        DispatchQueue.main.async { [weak self] in
            
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let productsViewController = storyboard.instantiateViewController(withIdentifier: String(describing: ProductsViewController.self))
            let navigationController = UINavigationController(rootViewController: productsViewController)
            
            self?.viewController?.present(navigationController, animated: true, completion: nil)
        }
    }
}
