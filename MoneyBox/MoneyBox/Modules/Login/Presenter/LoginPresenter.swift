//
//  LoginLoginPresenter.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

class LoginPresenter: LoginModuleInput, LoginViewOutput, LoginInteractorOutput {

    weak var view: LoginViewInput!
    var interactor: LoginInteractorInput!
    var router: LoginRouterInput!

    func viewIsReady() {
        view.setupInitialState()
    }
    
    // MARK: LoginViewOutput
    
    func login() {
        
        view.showSpinner()
        
        guard let email = self.view.email, let password = self.view.password, let idfa = self.view.idfa else {
            loginFailure(with: nil)
            return
        }
        
        interactor.login(email: email, password: password, idfa: idfa)
    }
    
    // MARK: LoginInteractorOutput
    
    func loginSuccess() {
        
        DispatchQueue.main.async { [weak self] in
            self?.view.hideSpinner()
        }
        router.presentProductList()
    }
    
    func loginFailure(with error: Error?) {
        
        DispatchQueue.main.async { [weak self] in
            self?.view.hideSpinner()
            self?.view.showError(error: error)
        }
    }
}
