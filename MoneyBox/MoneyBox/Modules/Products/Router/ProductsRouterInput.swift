//
//  ProductsProductsRouterInput.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import UIKit

protocol ProductsRouterInput {

    weak var viewController :UIViewController? { get set }

    func dismissProducts()
    
    func showProductDetails(product :Product)
}
