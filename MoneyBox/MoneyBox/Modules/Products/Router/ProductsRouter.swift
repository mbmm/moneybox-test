//
//  ProductsProductsRouter.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import UIKit

class ProductsRouter: ProductsRouterInput {

    weak var viewController :UIViewController?
    
    func dismissProducts() {
        
        DispatchQueue.main.async { [weak self] in
            self?.viewController?.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    
    func showProductDetails(product :Product) {
        
        DispatchQueue.main.async { [weak self] in
            
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let productDetailViewController = storyboard.instantiateViewController(withIdentifier: String(describing: ProductDetailViewController.self)) as! ProductDetailViewController
            
            let configurator = ProductDetailModuleConfigurator()
            configurator.product = product
            configurator.configureModuleForViewInput(viewInput: productDetailViewController)
            self?.viewController?.show(productDetailViewController, sender: nil)
        }
    }
}
