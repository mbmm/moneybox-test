//
//  ProductsProductsPresenter.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

class ProductsPresenter: ProductsModuleInput, ProductsViewOutput, ProductsInteractorOutput {

    weak var view: ProductsViewInput!
    var interactor: ProductsInteractorInput!
    var router: ProductsRouterInput!

    func viewIsReady() {
        view.setupInitialState()
        interactor.loadProducts()
    }
    
    // MARK: ProductsViewOutput
    
    func logout() {
        interactor.logout()
    }
    
    // MARK: ProductsInteractorOutput
    
    func logoutSuccess() {
        router.dismissProducts()
    }
    
    func logoutFailure(with error: Error?) {
        DispatchQueue.main.async { [weak self] in
            self?.view.showError(error: error)
        }
    }
    
    func productsLoaded() {
        DispatchQueue.main.async { [weak self] in
            self?.view.reloadProducts()
        }
    }
    
    func productsFailed(with error: Error?) {
        DispatchQueue.main.async { [weak self] in
            self?.view.showError(error: error)
        }
    }
    
    func section() -> Int {
        
        return interactor?.products != nil ? 1 : 0
    }
    
    func numberOfProducts(section: Int) -> Int {
        
        guard let productCout = interactor?.products?.count else {
            return 0
        }
        
        return productCout
    }
    
    func productName(section: Int, index: Int) -> String? {
        
        guard let product = interactor?.products?[index] else {
            return nil
        }
        
        return product.friendlyName
    }
    
    func productSelected(section: Int, index: Int) {
        
        guard let product = interactor?.products?[index] else {
            return
        }
        router.showProductDetails(product: product)
    }
}
