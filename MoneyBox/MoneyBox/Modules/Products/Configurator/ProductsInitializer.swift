//
//  ProductsProductsInitializer.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import UIKit

class ProductsModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var productsViewController: ProductsViewController!

    override func awakeFromNib() {

        let configurator = ProductsModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: productsViewController)
    }

}
