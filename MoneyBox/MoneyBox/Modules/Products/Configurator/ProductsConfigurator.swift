//
//  ProductsProductsConfigurator.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import UIKit

class ProductsModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? ProductsViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: ProductsViewController) {

        let router = ProductsRouter()
        router.viewController = viewController
        
        let presenter = ProductsPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = ProductsInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
