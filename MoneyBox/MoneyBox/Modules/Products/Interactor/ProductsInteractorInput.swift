//
//  ProductsProductsInteractorInput.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

protocol ProductsInteractorInput {

    func logout()
    func loadProducts()
    
    var products :[Product]? { get set }
}
