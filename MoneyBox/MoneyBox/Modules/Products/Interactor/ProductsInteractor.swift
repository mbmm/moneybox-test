//
//  ProductsProductsInteractor.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

class ProductsInteractor: ProductsInteractorInput {

    weak var output: ProductsInteractorOutput!

    var logoutTask :APITask?
    var loadProductsTask :APITask?
    var products :[Product]?
    
    deinit {
        logoutTask?.cancel()
        loadProductsTask?.cancel()
    }
    
    // MARK: ProductsInteractorInput
    
    func loadProducts() {
        
        loadProductsTask?.cancel()
        
        do {
            
            loadProductsTask = try APIClient.shared.thisWeekInvestorProduct({ [weak self] (success :Bool, result :ThisWeekInvestorProductResult?, error :Error?) in
                
                if success == true {
                    self?.products = result?.products
                    self?.output.productsLoaded()
                } else {
                    self?.output.productsFailed(with: error)
                }
            })
        }
        catch {
            self.output.productsFailed(with: error)
        }
    }
    
    func logout() {
        
        logoutTask?.cancel()
        
        do {
            logoutTask = try APIClient.shared.logout({ [weak self] (success :Bool, error :Error?) in
                
                if success == true {
                    self?.output.logoutSuccess()
                } else {
                    self?.output.logoutFailure(with: error)
                }
            })
        }
        catch {
            self.output.logoutFailure(with: error)
        }
    }
}
