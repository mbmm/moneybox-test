//
//  ProductsProductsInteractorOutput.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import Foundation

protocol ProductsInteractorOutput: class {

    func logoutSuccess()
    func logoutFailure(with error: Error?)
    func productsLoaded()
    func productsFailed(with error: Error?)
}
