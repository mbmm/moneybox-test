//
//  ProductsProductsViewInput.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

protocol ProductsViewInput: class {

    /**
        @author Manuele Maggi
        Setup initial state of the view
    */

    func setupInitialState()
    
    func showError(error :Error?)
    
    func reloadProducts()
}
