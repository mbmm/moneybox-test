//
//  ProductsProductsViewController.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import UIKit

class ProductsViewController: UIViewController, ProductsViewInput, UITableViewDelegate, UITableViewDataSource {

    var output: ProductsViewOutput!

    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }

    // MARK: ProductsViewInput
    func setupInitialState() {
        
        self.title = NSLocalizedString("Products", comment: "")
        
        let logoutButton = UIBarButtonItem(title: NSLocalizedString("Logout", comment: ""),
                                           style: UIBarButtonItemStyle.plain,
                                           target: self,
                                           action: #selector(logoutButtonAction))
        
        self.navigationItem.leftBarButtonItem = logoutButton
    }
    
    func showError(error: Error?) {
        self.showError(message: "Products failed!!!")
    }
    
    func reloadProducts() {
        
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    // MARK: NavigationBar actions
    
    @objc func logoutButtonAction() {
        self.output.logout()
    }
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return output.section()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.numberOfProducts(section: section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let productName = output.productName(section: indexPath.section, index: indexPath.row)
        let productCell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProductTableViewCell.self), for: indexPath) as? ProductTableViewCell
        productCell?.name = productName
        
        return UITableViewCell()
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        output.productSelected(section: indexPath.section, index: indexPath.row)
        tableView.reloadData()
    }
}
