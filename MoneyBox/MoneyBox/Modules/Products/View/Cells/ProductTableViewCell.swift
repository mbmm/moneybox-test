//
//  ProductTableViewCell.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    
    @IBOutlet weak var cornerView: UIView!
    @IBOutlet weak var productNameLabel: UILabel!
    
    var name :String? {
        
        get { return productNameLabel?.text }
        set { productNameLabel?.text = newValue }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        cornerView.layer.cornerRadius = 5.0
        cornerView.layer.borderWidth = 2.0
        contentView.layer.borderColor = UIColor.blue.cgColor
    }
}
