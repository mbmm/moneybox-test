//
//  ProductsProductsViewOutput.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

protocol ProductsViewOutput {

    /**
        @author Manuele Maggi
        Notify presenter that view is ready
    */

    func viewIsReady()
    
    func logout()
    
    func section() -> Int
    
    func numberOfProducts(section :Int) -> Int
    
    func productName(section :Int, index :Int) -> String?
    
    func productSelected(section :Int, index :Int)
}
