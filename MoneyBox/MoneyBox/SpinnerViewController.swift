//
//  SpinnerViewController.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 27/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import UIKit
import Lottie

class SpinnerViewController: UIViewController {
    
    var animationView: LOTAnimationView = LOTAnimationView(name: "magician");
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        animationView.contentMode = .scaleAspectFill
        animationView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(animationView)
        
        let centerX = NSLayoutConstraint(item: animationView, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        
        let centerY = NSLayoutConstraint(item: animationView, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1.0, constant: 0.0)
        
        self.view.addConstraints([centerX, centerY])
        
        animationView.loopAnimation = true
        animationView.play()
    }
}
