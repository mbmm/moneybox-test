//
//  APITaskLoginTests.swift
//  MoneyBoxTests
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import XCTest
@testable import MoneyBox

class APITaskLoginTests: XCTestCase {
    
    let builder = APIRequestBuilder(URL(string: "https://api.moneybox.com")!, "8cb2237d0679ca88db6464", "4.0.0", "3.0.0")
    let email = "test+sample@moneyboxapp.com"
    let password = "Password123"
    let idfa = "xxx"
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLoginSuccess() {

        let expectedToken = "Kcuf/DOjXgwDioE6wOdM1XyR/+ncwzdT0N9bJjl+O6g="
        
        let responseFileURL = Bundle(for: type(of: self)).url(forResource: "LoginSuccess", withExtension: "json")
        let responseData = try? Data(contentsOf: responseFileURL!)
        
        let urlResponse = HTTPURLResponse(url: URL(string: "https://api.moneybox.com/users/login")!,
                                          statusCode: 200,
                                          httpVersion: "HTTP/1.1",
                                          headerFields: nil)
        
        let mockSession = URLSessionMock(data: responseData,
                                         response: urlResponse,
                                         error: nil)
        
        let loginTask = APITaskLogin(with: email, password, idfa, builder: builder)
        
        loginTask.session = mockSession
        loginTask.completionHandler = { [weak self] (_ success :Bool, _ result :Any?, _ error :Error?) in
            
            guard let loginResult = result as? LoginResult else {
                XCTFail("No login result object")
                return
            }
            
            XCTAssertTrue(success, "success must be true")
            XCTAssertNil(error, "error must be nil")
            XCTAssertTrue(loginResult.user.email == self?.email, "email parameter doesn't match the response")
            XCTAssertTrue(loginResult.session.bearerToken == expectedToken, "expected token doesn't match the response")
        }
        
        loginTask.perform()
    }
    
    func testLoginFailure() {
        
        let urlResponse = HTTPURLResponse(url: URL(string: "https://api.moneybox.com/users/login")!,
                                          statusCode: 401,
                                          httpVersion: "HTTP/1.1",
                                          headerFields: nil)
        
        let mockSession = URLSessionMock(data: nil,
                                         response: urlResponse,
                                         error: nil)
        
        let loginTask = APITaskLogin(with: email, password, idfa, builder: builder)
        
        loginTask.session = mockSession
        loginTask.completionHandler = { (_ success :Bool, _ result :Any?, _ error :Error?) in
            
            XCTAssertFalse(success, "success must be true")

            let taskError = error as? APITaskError
            XCTAssertNotNil(taskError, "error must be nil")

            switch taskError! {
                
            case APITaskError.statusCode(code: 401): break
                
            default:
                XCTFail("status code is different than expected")
            }
        }
        
        loginTask.perform()
    }
}
