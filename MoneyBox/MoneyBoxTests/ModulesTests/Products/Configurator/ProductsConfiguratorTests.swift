//
//  ProductsProductsConfiguratorTests.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import XCTest
@testable import MoneyBox

class ProductsModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = ProductsViewControllerMock()
        let configurator = ProductsModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "ProductsViewController is nil after configuration")
        XCTAssertTrue(viewController.output is ProductsPresenter, "output is not ProductsPresenter")

        let presenter: ProductsPresenter = viewController.output as! ProductsPresenter
        XCTAssertNotNil(presenter.view, "view in ProductsPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in ProductsPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is ProductsRouter, "router is not ProductsRouter")

        let interactor: ProductsInteractor = presenter.interactor as! ProductsInteractor
        XCTAssertNotNil(interactor.output, "output in ProductsInteractor is nil after configuration")
    }

    class ProductsViewControllerMock: ProductsViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
