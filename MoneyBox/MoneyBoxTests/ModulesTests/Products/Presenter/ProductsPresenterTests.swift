//
//  ProductsProductsPresenterTests.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import XCTest
@testable import MoneyBox

class ProductsPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: ProductsInteractorInput {
        func logout() {
            
        }
        
        func loadProducts() {
            
        }
        
        var products: [Product]?
    }

    class MockRouter: ProductsRouterInput {
        var viewController: UIViewController?
        
        func dismissProducts() {
            
        }
        
        func showProductDetails(product: Product) {
            
        }
    }

    class MockViewController: ProductsViewInput {
        func showError(error: Error?) {
            
        }
        
        func reloadProducts() {
            
        }
        
        func setupInitialState() {

        }
    }
}
