//
//  ProductsProductsInteractorTests.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 26/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import XCTest
@testable import MoneyBox

class ProductsInteractorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockPresenter: ProductsInteractorOutput {
        func logoutSuccess() {
            
        }
        
        func logoutFailure(with error: Error?) {
            
        }
        
        func productsLoaded() {
            
        }
        
        func productsFailed(with error: Error?) {
            
        }
        

    }
}
