//
//  ProductDetailProductDetailPresenterTests.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 27/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import XCTest
@testable import MoneyBox

class ProductDetailPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: ProductDetailInteractorInput {
        var product: Product?
        
        var moneyBox: Double?
        
        func makePayment(of amount: Double) {
            
        }
        
        func setProduct(product: Product) {
            
        }
    }

    class MockRouter: ProductDetailRouterInput {

    }

    class MockViewController: ProductDetailViewInput {
        func reloadMoneyBox() {
            
        }
        
        func showError(error: Error?) {
            
        }
        
        func showSpinner() {
            
        }
        
        func hideSpinner() {
            
        }

        func setupInitialState() {

        }
    }
}
