//
//  ProductDetailProductDetailConfiguratorTests.swift
//  MoneyBox
//
//  Created by Manuele Maggi on 27/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import XCTest
@testable import MoneyBox

class ProductDetailModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = ProductDetailViewControllerMock()
        let configurator = ProductDetailModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "ProductDetailViewController is nil after configuration")
        XCTAssertTrue(viewController.output is ProductDetailPresenter, "output is not ProductDetailPresenter")

        let presenter: ProductDetailPresenter = viewController.output as! ProductDetailPresenter
        XCTAssertNotNil(presenter.view, "view in ProductDetailPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in ProductDetailPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is ProductDetailRouter, "router is not ProductDetailRouter")

        let interactor: ProductDetailInteractor = presenter.interactor as! ProductDetailInteractor
        XCTAssertNotNil(interactor.output, "output in ProductDetailInteractor is nil after configuration")
    }

    class ProductDetailViewControllerMock: ProductDetailViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
