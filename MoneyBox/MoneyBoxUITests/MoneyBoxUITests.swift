//
//  MoneyBoxUITests.swift
//  MoneyBoxUITests
//
//  Created by Manuele Maggi on 25/11/2017.
//  Copyright © 2017 Manuele Maggi. All rights reserved.
//

import XCTest

class MoneyBoxUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLogin() {
        
        let app =  XCUIApplication()
        let emailTextField = app.textFields["Email"]
        wait(element: emailTextField)
        emailTextField.tap()
        emailTextField.typeText("test+env02@moneyboxapp.com")
        
        let passwordSecureTextField = app.secureTextFields["Password"]
        wait(element: passwordSecureTextField)
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText("Money$$box@107")
        app.buttons["Login"].tap()
        
        let productsScreen = app.navigationBars["Products"].otherElements["Products"]
        
        wait(element: productsScreen)
    }
    
    func wait(element :XCUIElement) {
        
        let exists = NSPredicate(format: "exists == true")
        expectation(for: exists, evaluatedWith: element, handler: nil)
        waitForExpectations(timeout: 10, handler: nil)
        XCTAssert(element.exists)
    }
    
    
}
